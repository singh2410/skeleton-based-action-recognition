#!/usr/bin/env python
# coding: utf-8

# # Skeleton Based Action Recognition
# #By- Aarush Kumar
# #Dated: October 11,2021

# In[2]:


from IPython.display import Image
Image(url='https://raw.githubusercontent.com/fabro66/Online-Skeleton-based-Action-Recognition/master/images/ntu-gast-skeleton.png')


# In[3]:


import os
import pandas as pd
import numpy as np
import seaborn as sns
from scipy.io import loadmat
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB, GaussianNB
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from xgboost import XGBClassifier
from lightgbm import LGBMClassifier
import warnings
warnings.filterwarnings("ignore")


# In[4]:


train = []
test = []
dataDir = "/home/aarush100616/Downloads/Projects/SKELETON BASED ACTION RECOGNITION/Data/"
for file in os.listdir(dataDir):
    if file.endswith(".mat"):
        if "s2" in file or "s4" in file or "s6" in file or "s8" in file:
            train.append([loadmat(dataDir+file)["d_skel"],file.split("_")[0]])
        else:
            test.append([loadmat(dataDir+file)["d_skel"],file.split("_")[0]])


# In[5]:


def trim(skl):
    val = skl.shape[2]
    if val > 40:
        if val>120:
            skl = skl[:,:,::3]
        elif val>80:
            skl = skl[:,:,::2]
        val = skl.shape[2]
        split = val - 40
        split = int(split/2)
        skl = skl[:,:,split:split+40]
        return np.array(skl)


# In[6]:


print(test[-1][0].shape)
print(trim(test[-1][0]).shape)


# In[7]:


d = lambda x1,y1,z1,x2,y2,z2 : ((x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2)**0.5
def dist_func(skl):
    dvals = d(skl[:,0,:],skl[:,1,:],skl[:,2,:],skl[1,0,:],skl[1,1,:],skl[1,2,:])
    dvals = (dvals[:]-dvals.mean())/dvals.std()
    return dvals


# In[8]:


x = train[0][0][:,0,0]
y = train[0][0][:,1,0]
z = train[0][0][:,2,0]


# In[9]:


val = np.array(d(x,y,z,x[1],y[1],z[1]))
(val-val.mean())/val.std()


# In[10]:


np.array([dist_func(train[0][0])[i][0] for i in range(20)])


# In[11]:


def return_distance_and_action(mats):
    x = []
    y = []
    for skl in mats:
        x.append(dist_func(trim(skl[0])).flatten())
        y.append(skl[1])
    return np.array(x),np.array(y)


# In[12]:


X_train,y_train = return_distance_and_action(train)
X_test,y_test = return_distance_and_action(test)


# In[13]:


lr = LogisticRegression(solver='newton-cg')
lr.fit(X_train, y_train)
y_pred = lr.predict(X_test)
print("Accuracy: ",(accuracy_score(y_test,y_pred)))


# In[14]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# In[15]:


knn = KNeighborsClassifier(n_neighbors = 3)
knn.fit(X_train, y_train)
y_pred = knn.predict(X_test)
print("Accuracy: ",accuracy_score(y_test,y_pred))


# In[16]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# In[17]:


mnb = MultinomialNB()
ms = MinMaxScaler()
mnb.fit(ms.fit_transform(X_train), y_train)
y_pred = mnb.predict(ms.fit_transform(X_test))
print("Accuracy: ",accuracy_score(y_test,y_pred))


# In[18]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# In[19]:


gnb = GaussianNB()
gnb.fit(X_train, y_train)
y_pred = gnb.predict(X_test)
print("Accuracy: ",accuracy_score(y_test,y_pred))


# In[20]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# In[21]:


xgb = XGBClassifier()
xgb.fit(X_train, y_train)
y_pred = xgb.predict(X_test)
print("Accuracy: ",accuracy_score(y_test,y_pred))


# In[22]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# In[23]:


etc = ExtraTreesClassifier()
etc.fit(X_train, y_train)
y_pred = etc.predict(X_test)
print("Accuracy: ",accuracy_score(y_test,y_pred))


# In[24]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# In[25]:


dtc = DecisionTreeClassifier()
dtc.fit(X_train, y_train)
y_pred = dtc.predict(X_test)
print("Accuracy: ",accuracy_score(y_test,y_pred))


# In[26]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# In[27]:


lightgbm = LGBMClassifier()
lightgbm.fit(X_train, y_train)
y_pred = lightgbm.predict(X_test)
print("Accuracy: ",accuracy_score(y_test,y_pred))


# In[28]:


sns.heatmap(confusion_matrix(y_test,y_pred))


# * We have successfully implemented skeleton based action recognition using data preprocessing techniques for feature extraction by converting all the d_skel data stored in the .mat files into 1-D array and finally building a classification models using machine learning techniques
